return {
    -- dependency for emmet
    {'mattn/webapi-vim'},

    -- great html shortcuts
    {'mattn/emmet-vim'},

    -- comment shortcuts
    {'numToStr/Comment.nvim'},

    -- auto insert matching brackets and quotes
    {'windwp/nvim-autopairs'},

    -- for surrounding text with delimiters such as brackets and quotes
    {'echasnovski/mini.surround'},

    -- For quickly switching between camel and snake case etc
    {'arthurxavierx/vim-caser'},
}
