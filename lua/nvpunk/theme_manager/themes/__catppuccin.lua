--- Set catpuccin theme with specific style
---@param style 'macchiato' | 'latte' | 'frappe' | 'macchiato' | 'mocha'
return function(style)
    if style == 'latte' then
        vim.o.background = 'light'
    else
        vim.o.background = 'dark'
    end
    vim.g.catppuccin_flavour = style
    local t = require'catppuccin'
    t.setup {
        dim_inactive = {
            enabled = true,
            shade = 'dark',
            percentage = 0.15
        },
        term_colors = true,
        integrations = {
            treesitter = true,
            native_lsp = {
                enabled = true,
            },
            coc_nvim = false,
            lsp_trouble = true,
            cmp = true,
            lsp_saga = true,
            gitgutter = true,
            gitsigns = true,
            telescope = true,
            nvimtree = true,
            neotree = true,
            dap = {
                enabled = true, enable_ui = true,
            },
            which_key = true,
            indent_blankline = {
                enabled = true,
                colored_indent_levels = false,
            },
            dashboard = false,
            neogit = false,
            vim_sneak = false,
            fern = false,
            markdown = true,
            lightspeed = false,
            ts_rainbow = false,
            hop = false,
            mason = true,
            notify = true,
            symbols_outline = true,
            mini = true,
            vimwiki = false,
            beacon = false,
            navic = { enabled = true, custom_bg = 'NONE' },
            overseer = false,
            aerial = true,
        },
    }
    t.load()
    vim.cmd[[colorscheme catppuccin]]
    reload'nvpunk.theme_manager.lualine'('catppuccin')
end
